# nuxt-app-in-docker
This is the starting template for NuxtJS development.

Run in console:
```
docker-compose up
```
Wait until the 'npm install' is finished, then open:
```
http://localhost:3000/
```

