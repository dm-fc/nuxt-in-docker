module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', '.'],
        ],
        extensions: ['.vue', '.js', '.ts'],
      },
    },
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@nuxtjs/eslint-config-typescript',
    'eslint-config-airbnb',
    // 'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {},
};
